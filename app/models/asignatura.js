var mongoose = require('mongoose'),Schema = mongoose.Schema;

var userSchema = new Schema({
    nombre: String,
    horas: String,
    docente: Object,
    alumnos: array[Object]
});

module.exports = mongoose.model('asignatura', userSchema);