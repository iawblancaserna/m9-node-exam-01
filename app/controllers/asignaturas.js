// var Student = require("../models/alumnos");
// var Teacher = require("../models/docente");
var Asignatura = require("../models/asignaturas");

// Cogemos de la bd los alumnos i profes para ponerlos en un select
var getAlumnosTeacher = async (req, res, next) => {
    [

    ]
};

// All teachers
var findTeachers = (req, res) => {
    Teacher.find(function (err, teachers) {
        return teachers;
    });
};

// All students
var findStudents = (req, res) => {
    Student.find(function (err, students) {
        return students;
    });
};

// Llistar

var listSubjects = async (req, res) => {
    Asignatura.find(function (err, subjects) {
        return subjects;
    });
};
// Crear

var createSubject = async (req, res) => {
    var subject = new Asignatura({ nombre: req.body.name, horas: req.body.horas, docentes: req.body.selectDocent, alumnos: req.body.checkAlumnos });
    //console.log(subject);
    subject.save(); // la guardem a la bd
    return subject;
};
// Editar

var editSubject = async (req, res) => {

    var subject = Asignatura.findOne({ nombre: req.body.name }, function (err, subject) {
        if (err) {
            console.log("Error en la busqueda d'aquesta asignatura");
        }

        if (!subject) {
            console.log("No s'ha trobat aquesta asignatura");
        }
        // agafem 
        subject.nombre = req.body.name;
        subject.horas = req.body.horas;
        subject.docent = req.body.selectDocents;
        subject.alumnos = req.body.checkAlumnos;

    });
    subject.save; // el guardem ara editat ja
};

// Eliminar (by name)

var deleteSubject = async (req, res) => {
    Asignatura.deleteOne({ nombre: req.body.name });
    console.log("S'ha eliminat l'asignatura");
}


//findOne({ nombre: req.body.name }, function (error, subject) {

module.exports = {
    getAlumnosTeacher,
    findTeachers,
    findStudents
};