$(document).ready(function () {
  //Inicializa socket con IO
  // socket("connected") => {

  // }
  //Cuando cambia el select redirigimos a la URL del chat
  $('#selectRoom').on("change",()=>{
    var sala = $(this).find("option:selected").val();
    window.location.href = "/chat/"+sala;
  })
  
  //Accion cuando el usuario envia mensaje con submit
  $("#chat").submit(function (e) {
    e.preventDefault();
    var msg = $("#msg").val();
    $("#chatBox").append(`<p>${msg}<p>`);
  });

  //Acciones a realizar cuando se detecta actividad en el canal newMsgp
  // socket.on("chat", msg => {

  // });
//   document.getElementById("buttonSubmit").addEventListener("submit") {
//     let msg = document.getElementById("inputMsg").value;
//     let checkbox = document.getElementById('checkBox');
//     console.log(msg);
//     checkbox.value += 'User: ' + msg;
// }
  // socket.on("broadcast", msg => {

  // };
});
