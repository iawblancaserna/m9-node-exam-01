var express = require("express");
var path = require("path");
var router = express.Router();

router.get("/", async (req, res, next) => {
    res.render("index.pug");
});

// cargar vista new asignatura
router.get("/newAsignatura", async (req, res, next) => {
    res.render("newAsignatura.pug");
});
//res.render("newAsignatura", {docent: req.body.docent});
module.exports = router;