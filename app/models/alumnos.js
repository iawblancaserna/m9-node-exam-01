var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var userSchema = new Schema({
    nombre: String,
    apellido: String
});

module.exports = mongoose.model('alumnos', userSchema);